/**
 * Server side code.
 */
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));

function clone() {
    var Employee = {
        firstName: "James",
        lastName: "Bond",
        employeeNumber: 007,
        salary: 1000000,
        age: 35,
        department: "SIS",
      
    };
    
    var Person = {};
    var Person = Object.assign(Person, Employee);
    
    console.log("Age: " + Person.age);
    
    }
    
    clone();
    
    
    

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});